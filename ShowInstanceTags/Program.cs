﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowInstanceTags
{
    class Program
    {
        static void Main(string[] args)
        {
            // This doesn't require ec2:DescribeInstances IAM permissions

            String MyInstanceID = Amazon.Util.EC2InstanceMetadata.InstanceId.ToString();
            String MyType = Amazon.Util.EC2InstanceMetadata.InstanceType;

            Console.WriteLine("My Instance ID [{0}]", MyInstanceID);
            Console.WriteLine("I'm running on a [{0}]", MyType);

            // This *WILL* require ec2:DescribeInstances IAM permissions for the role (or access keys, etc.):
            //
            // The policy to allow this could be something as simple as:
            //
            //            {
            //               "Version": "2012-10-17",
            //               "Statement": [
            //                  {
            //                  "Sid": "Stmt1499288332000",
            //                  "Effect": "Allow",
            //                  "Action": [
            //                     "ec2:DescribeInstances"
            //                     ],
            //                  "Resource": [
            //                     "*"
            //                     ]
            //                  }
            //               ]
            //            }

            Amazon.EC2.AmazonEC2Client ec2 = new Amazon.EC2.AmazonEC2Client();
            // Let's define a filter (so we only get our instance back)
            Amazon.EC2.Model.DescribeInstancesRequest ec2describe = new Amazon.EC2.Model.DescribeInstancesRequest();
            ec2describe.InstanceIds.Add(MyInstanceID);
            // Describe instances (If you take out the filter you'll get the output for all instances)
            // This returns the lists, as if we have an unknown number of instances

            Amazon.EC2.Model.DescribeInstancesResponse ec2response = ec2.DescribeInstances(ec2describe);
            // Reservations are the act of launching (if you from sdk or web console launched 5 instances then all 5 are part of 1 reservation)
            foreach (Amazon.EC2.Model.Reservation reservation in ec2response.Reservations)
            {
                // This is the one that's more logical to most as usually reservations only have one instance:
                foreach (Amazon.EC2.Model.Instance instance in reservation.Instances)
                {
                    Console.WriteLine("Found match in Instance [{0}]", instance.InstanceId);
                    foreach (Amazon.EC2.Model.Tag tag in instance.Tags)
                    {
                        Console.WriteLine("   Tags: Key [{0}] = [{1}]", tag.Key, tag.Value);
                    }
                }
            }
        }
    }
}
